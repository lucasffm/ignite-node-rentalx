/* eslint-disable no-unused-vars */
import express, { NextFunction, Request, Response } from 'express';
import router from './routes';

const app = express();

app.use(express.json());

app.get('', (req, res) => {
  res.json({ message: 'Hello World' });
});

app.use(router);

app.use((err: any, req: Request, res: Response, next: NextFunction) => {
  console.error(err.stack);
  res.status(500).send({ message: err.message });
  next();
});

app.listen(3333, () => {
  console.log('Server is running!!!');
});
