import { Request, Response } from 'express';
import CreateSpecificationUseCase from './CreateSpecificationUseCase';

export default class CreateSpecificationController {
  constructor(private uc: CreateSpecificationUseCase) { }

  handle(request: Request, response: Response): Response {
    const { name, description } = request.body;
    const specification = this.uc.execute({ name, description });
    return response.status(201).send(specification);
  }
}
