import { ISpecificationsRepository } from '../../repositories/ISpecificationsRepository';

interface IRequest {
  name: string
  description:string
}

export default class CreateSpecificationUseCase {
  constructor(private repo: ISpecificationsRepository) {}

  execute({ name, description }: IRequest) {
    const alreadyExists = this.repo.findByName(name);
    if (alreadyExists) {
      throw new Error('Specification already exists!!!');
    }

    return this.repo.create({ name, description });
  }
}
