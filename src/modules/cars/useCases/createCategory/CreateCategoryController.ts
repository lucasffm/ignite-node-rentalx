import { Request, Response } from 'express';
import CreateCategoryUseCase from './CreateCategoryUseCase';

export default class CreateCategoryController {
  constructor(private uc: CreateCategoryUseCase) { }

  handle(request: Request, response: Response): Response {
    const { name, description } = request.body;
    const category = this.uc.execute({ name, description });
    return response.status(201).send(category);
  }
}
