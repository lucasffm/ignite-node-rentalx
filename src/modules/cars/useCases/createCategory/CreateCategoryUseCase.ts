import { ICategoryRepository } from '../../repositories/ICategoryRepository';

interface IRequest {
  name: string
  description:string
}

export default class CreateCategoryUseCase {
  constructor(private repo: ICategoryRepository) {}

  execute({ name, description }: IRequest) {
    const alreadyExists = this.repo.findByName(name);
    if (alreadyExists) {
      throw new Error('Category already exists!!!');
    }

    return this.repo.create({ name, description });
  }
}
