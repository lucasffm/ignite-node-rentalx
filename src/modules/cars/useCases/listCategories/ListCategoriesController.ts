import { Request, Response } from 'express';
import ListCategoriesUseCase from './ListCategoriesUseCase';

export default class ListCategoriesController {
  constructor(private uc: ListCategoriesUseCase) { }

  handle(request: Request, response: Response): Response {
    const category = this.uc.execute();
    return response.status(200).send(category);
  }
}
