import { ICategoryRepository } from '../../repositories/ICategoryRepository';

export default class ListCategoriesUseCase {
  constructor(private repo: ICategoryRepository) {}

  execute() {
    return this.repo.list();
  }
}
