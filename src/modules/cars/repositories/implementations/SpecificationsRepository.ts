import Specification from '../../model/Specification';
import { ICreateSpecificationDTO, ISpecificationsRepository } from '../ISpecificationsRepository';

export default class SpecificationsRespository implements ISpecificationsRepository {
  private specifications: Specification[];

  private static INSTANCE: SpecificationsRespository;

  constructor() {
    this.specifications = [];
  }

  public static getInstance(): SpecificationsRespository {
    if (!SpecificationsRespository.INSTANCE) {
      SpecificationsRespository.INSTANCE = new SpecificationsRespository();
    }
    return SpecificationsRespository.INSTANCE;
  }

  create({ name, description }: ICreateSpecificationDTO): Specification {
    const specification = new Specification();
    Object.assign(specification, {
      name, description, created_at: new Date(),
    });

    this.specifications.push(specification);
    return specification;
  }

  list(): Specification[] {
    return this.specifications;
  }

  findByName(name: string): Specification {
    return this.specifications.find((specification) => specification.name === name);
  }
}
