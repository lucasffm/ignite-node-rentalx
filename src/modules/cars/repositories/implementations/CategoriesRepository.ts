import Category from '../../model/Category';
import { ICategoryRepository, ICreateCategoryDTO } from '../ICategoryRepository';

export default class CategoriesRespository implements ICategoryRepository {
  private categories: Category[];

  private static INSTANCE: CategoriesRespository;

  private constructor() {
    this.categories = [];
  }

  public static getInstance(): CategoriesRespository {
    if (!CategoriesRespository.INSTANCE) {
      CategoriesRespository.INSTANCE = new CategoriesRespository();
    }
    return CategoriesRespository.INSTANCE;
  }

  create({ name, description }: ICreateCategoryDTO): Category {
    const category = new Category();
    Object.assign(category, {
      name, description, created_at: new Date(),
    });

    this.categories.push(category);
    return category;
  }

  list(): Category[] {
    return this.categories;
  }

  findByName(name: string): Category {
    return this.categories.find((category) => category.name === name);
  }
}
