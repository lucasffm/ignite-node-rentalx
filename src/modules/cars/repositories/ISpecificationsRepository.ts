import Specification from '../model/Specification';

export interface ICreateSpecificationDTO {
  name: string
  description: string
}

export interface ISpecificationsRepository {
  create(params: ICreateSpecificationDTO): Specification;
  list(): Specification[];
  findByName(name: string): Specification;
}
