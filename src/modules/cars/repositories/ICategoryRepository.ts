import Category from '../model/Category';

export interface ICreateCategoryDTO {
  name: string
  description: string
}

export interface ICategoryRepository {
  create(params: ICreateCategoryDTO): Category;
  list(): Category[];
  findByName(name: string): Category;
}
