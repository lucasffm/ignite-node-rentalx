import { Request, Response, Router } from 'express';
import SpecificationsRespository from '../modules/cars/repositories/implementations/SpecificationsRepository';
import { createSpecificationController } from '../modules/cars/useCases/createSpecification';

const specificationsRoutes = Router();
const repo = new SpecificationsRespository();

specificationsRoutes.post('', (req: Request, res: Response) => createSpecificationController.handle(req, res));

specificationsRoutes.get('', (req: Request, res: Response) => {
  const specifications = repo.list();

  return res.json({ items: specifications.length, specifications });
});

export default specificationsRoutes;
